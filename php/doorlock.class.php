<?php

/*
 *  description
 * 
 *  @category Smart Home Project Files
 *  @author Brody Bruns <brody.bruns@hotmail.com>
 *  @copyright (c) 2016, Brody Bruns
 *  @version 1.0
 * 
 */

/**
 * Description of doorlock
 *
 * @author Brody
 */
class Doorlock extends Database{
    protected $tableName = "doorlock";
    protected $fields = ['id','deviceName','state'];
    public $deviceName;
    public $state;
    
    /*
     * Object methods
     */
    public function openLock(){
        if(isset($this->id)){
            $error = (new VDev_Lock_Request())->openDoorLock($this->id);
            $error2 = (new VDev_Lock_Request())->setLevelMetricByDeviceId($this->id, "open");
        }
    }
    
    public function closeLock(){
        if(isset($this->id)){
            $error = (new VDev_Lock_Request())->closeDoorLock($this->id);
            $error2 = (new VDev_Lock_Request())->setLevelMetricByDeviceId($this->id, "close");
        }
    }
    
    /**
     * poll current state directly from zwave network
     * @return string or null
     */
    public function pollPresentState(){
        if(isset($this->id)){
            $metrics = (new VDev_Lock_Request())->getDeviceMetrics($this->id);
            return $metrics->level;
        }else{
            return null;
        }
    }   
    
    public function toggleLock(){
        $currentState = $this->pollPresentState();
        if($currentState === "close"){//if lock closed
            $this->openLock();//open lock
        }elseif($currentState === "open"){//else is open
            $this->closeLock();//close lock
        }else{
            //unknown state
        }
    }
    
    public function updateStateMetric($state){
        return (new VDev_Lock_Request())->setLevelMetricByDeviceId($this->id, $state);
    }
    
    /*
     * Static methods for building lock menu
     */
    public static function synchDatabaseLocks(){
        //get locks from api
        $result = (new VDev_Lock_Request())->getAllDoorLocksRequest();
        
        //create lock objects
        foreach($result->data as $lock_result){
            $doorlock = new self();
            $doorlock->id = $lock_result->deviceId;
            $doorlock->deviceName = $lock_result->deviceName;
            //set device level
            $doorlock->state = $doorlock->pollPresentState();
            $doorlock->save();//save each to database
        }        
        
    }
    
    public static function getLockForm(){
        //sync locks with db
        self::synchDatabaseLocks();
        //generate lock form from db values
        //foreach lock
            //get button
            //if level is 'closed'
                //use locked image
            //else (should be open)
                //use unlocked image
        $lockRecords = (new self())->getAllRecords();
        $form = "<div><h2>Door Lock</h2>";
        foreach($lockRecords as $lockRecord){
            $lock = new self();
            
            $lock->id = $lockRecord['id'];
            $lock->deviceName = $lockRecord['deviceName'];
            $lock->state = $lockRecord['state'];
            
            $button = $lock->getButtonDiv();
            $form .= "<div>$button</div>";
        }
        $form .= "</div>";
        
        return $form;
    }
    
    
    /**
     * 
     * @return string HTML Div of button and label
     */
    private function getButtonDiv(){    
        //add image based on status
        $source = "../images/open.JPG";
        $off_source = "../images/closed.JPG";
        
        $on_style = 'display:block;';
        $off_style = 'display:block;';
        
        if($this->state === 'open'){
            $off_style = 'display:none;';
        }else{
            $on_style = 'display:none;';
        }
        $on_image = "<img src='$source' height = '100' width='100' id='lock_open_$this->id' style='$on_style'>";
        $off_image = "<img src='$off_source' height = '100' width='100' id='lock_close_$this->id' style='$off_style'>";
        $button = "<div class='doorlock' data-id=$this->id>$this->deviceName $on_image $off_image</div>";
        return $button;
    }
    
}
