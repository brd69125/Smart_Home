<?php
require_once '../shell.php';
/**
 * this file contains ajax methods for doorlocks
 */

function toggleLock(){
    if(isset($_POST["ID"])){
        $lock_id = filter_input(INPUT_POST, "ID", FILTER_SANITIZE_STRING);
        $lock = new Doorlock();
        $lock->id = $lock_id;
        $currentState = $lock->pollPresentState(); //to return so we can check status
        $lock->toggleLock();
        echo $currentState;//will hopefully be past state, unlocking door takes awhile
    }else{
        echo "ERROR";
    }
}

function statusChangeOccurred(){
    $timeout = 300;//timeout in sec
    $timePassed = 0;
    $timeIntervals = 5; //every 5 seconds
    if(isset($_POST['ID']) && isset($_POST['original_status'])){
        $lock_id = filter_input(INPUT_POST, "ID", FILTER_SANITIZE_STRING);
        $original_state = filter_input(INPUT_POST, "original_status");
        $lock = new Doorlock();
        $lock->id = $lock_id;
        $currentState = $lock->pollPresentState();
        //check for state change, or timeout
        while($timePassed <= $timeout && $currentState != $original_state){
            //wait for time interval to check again
            sleep($timeIntervals);
            $timePassed += $timeIntervals;
            $currentState = $lock->pollPresentState();
        }
        
        if($currentState != $original_state){
            echo $currentState;
        }else{
            //timeout occurred, notify
            echo "TIMEOUT";
        }
    }else{
        echo "ERROR";
    }
}

/**
 * decide what interaction needs to be performed
 */
if(isset($_POST["interaction"])){
    $interaction = filter_input(INPUT_POST, "interaction", FILTER_SANITIZE_STRING);
    switch($interaction){ 
        case "toggle": 
            toggleLock();
            break;
        case "monitorStatusChange":
            statusChangeOccurred();
            break;
        default: //undefined interaction
            break;
    }
}

